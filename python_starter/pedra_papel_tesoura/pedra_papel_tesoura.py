import os
import random

opcoes = [
    "Pedra", 
    "Papel", 
    "Tesoura"]


def mostrar_lista_de_opcoes(lista):
    for i, opcao in enumerate(lista):
        print("[{}] - {}".format(i, opcao))
        
placar_computador = 0
placar_usuario = 0

while True: 
    
    os.system("clear")
    
    print("Escolha a opção que deseja:")
    mostrar_lista_de_opcoes(opcoes)
    
    try: 
        usuario_input = int(input())
        computador = random.randint(0,2)
        if usuario_input not in [0, 1, 2]: 
            raise
    except Exception as e:
        print(e)
        
    
    print("Você escolheu a opção {} e o computador escolheu {}".format(usuario_input, computador))
    print("============================")
    
    if usuario_input == computador:
        placar_usuario += 1
        placar_computador +=1
        print("Deu empate!")
    else:
        if usuario_input == 0 and computador == 1:
            placar_usuario += 1
            print("Usuário ganhou!")
        elif usuario_input == 0 and computador == 2:
            placar_usuario += 1
            print("Usuário ganhou!")
        elif usuario_input == 1 and computador == 0:
            placar_computador +=1
            print("Computador ganhou!")
        elif usuario_input == 2 and computador == 0:
            placar_computador +=1
            print("Computador ganhou!")
    
    print("============================")
    print("O placar está {} para o usuário e {} para o computador:".format(placar_usuario, placar_computador))
    print("============================")
    
    print("Deseja encerrar? Digite 0 para continuar e 1 para encerrar:")
    encerramento = int(input())
    
    if encerramento == 1:
     break